# @file 03 - test stage
# @description This file contains all mandatory or optionnal jobs that should be
# executed during the test stage of the gitlab toolchain CI/CD pipeline. It mostly consists
# of gitlab predefined templates that have been adapted to veolia workflow.

####################
# Secret detection #
####################

# @description Enable the secret_detection job inside your project.
#   The job will scan for leaking secrets inside your project and produce a report
#   that can be seen during your MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project,
#   once it has been merged on your main (single) or dev (multi) branch.
#   You can ignore test or fake secrets inside your project by adding  the `${PROJECT_DIR}/extended-gitleaks-config.toml` file
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SECRET_DETECTION_DISABLED variable is true
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/index.html)
secret_detection:
  variables:
    GIT_DEPTH: 0
  before_script:
    - !reference [.scripts, fetch_config_files_secret_detection_ultimate]
  rules:
    - !reference [".rules_secret_detection_ultimate", "rules"]

######################
# Container scanning #
######################
# Disable original ultimate container_scanning job
container_scanning:
  rules:
    - when: never

############
# IaC SAST #
############

# @description Enable the static IaC scanning job inside your project.
#   The job will use the KICS scanner to analyze terraform IaC files and check for terraform security best practices.
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/iac_scanning/)
kics-iac-sast:
  rules:
    - !reference [".rules_sast_default_ultimate", "rules"]

########
# SAST #
########

# @description Enable the static Helm charts scanning job inside your project.
#   The job will use the kubesec scanner to analyze helm charts files (YAML) and check for kubernetes security best practices.
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   By default the job is not enabled, you have to manually set the SCAN_KUBERNETES_MANIFESTS variable to true inside your `.gitlab-ci.yml` file in order to activate it.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#     - SCAN_KUBERNETES_MANIFESTS variable is false
#     - if no YAML files are found in your project
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/sast/)
kubesec-sast:
  rules:
    - !reference [".rules_test_sast_kubesec", "rules"]

# @description Enable the static Apex (Salesforce) scanning job inside your project.
#   The job will use the PMD scanner to analyze Apex files (.cls) and check for security best practices.
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#     - No **/*.cls file
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/sast/)
pmd-apex-sast:
  rules:
    - !reference [".rules_test_sast_pmd", "rules"]

# @description Enable static scanning job of most language/framework inside your project.
#   The job will use the semgrep scanner to analyze and check security best practices for :
#     - python
#     - java
#     - javascript
#     - typescript
#     - Node.js
#     - React
#     - C
#     - C++
#     - C#
#     - Objective-C
#     - Swift
#     - go
#     - html
#     - scala
#     - kotlin
#     - php
#     - Ruby
#     - Ruby on rails
#     - Rust
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#     - No file in:
#          - "**/*.py"
#          - "**/*.js"
#          - "**/*.jsx"
#          - "**/*.ts"
#          - "**/*.tsx"
#          - "**/*.c"
#          - "**/*.cc"
#          - "**/*.cpp"
#          - "**/*.c++"
#          - "**/*.cp"
#          - "**/*.cxx"
#          - "**/*.go"
#          - "**/*.java"
#          - "**/*.html"
#          - "**/*.cs"
#          - "**/*.scala"
#          - "**/*.sc"
#          - "**/*.php"
#          - "**/*.swift"
#          - "**/*.m"
#          - "**/*.rb"
#          - "**/*.kt"
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/sast/)
semgrep-sast:
  rules:
    - !reference [".rules_test_sast_semgrep", "rules"]

# @description Enable the static Elixir (Phoenix) scanning job inside your project.
#   The job will use the Sobelow scanner to analyze Elixir files (mix.exs) and check for security best practices.
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#     - No mix.exs file
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/sast/)
sobelow-sast:
  rules:
    - !reference [".rules_test_sast_sobelow", "rules"]

# @description Enable the static Groovy scanning job inside your project.
#   The job will use the Sobelow scanner to analyze groovy files (.groovy) and check for security best practices.
#   The output can be seen in MR result or in the [vulnerability report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/index.html) of your project.
#
#   This job is by default included with the toolchain.
#
#   By default this job will run on :
#
#     - Any commit on feature branch
#     - Any commit on main branch (single branch strategy)
#     - Any merge request on main branch (single branch strategy)
#     - Any commit on dev branch (multi branch strategy)
#     - Any merge request on dev branch (multi branch strategy)
#
#   By default this job will NOT run if:
#
#     - SAST_DISABLED variable is true
#     - No *.groovy file
#
#   More details on the gitlab predefined job [here](https://docs.gitlab.com/ee/user/application_security/sast/)
spotbugs-sast:
  rules:
    - !reference [".rules_test_sast_spotbugs", "rules"]

#######################
# Dependency Scanning #
#######################

gemnasium-dependency_scanning:
  variables:
    DS_MAX_DEPTH: -1
  rules:
    - !reference [".rules_test_dependency_scanning_gemnasium", "rules"]

gemnasium-maven-dependency_scanning:
  variables:
    DS_MAX_DEPTH: -1
  rules:
    - !reference [".rules_test_dependency_scanning_gemnasium_maven", "rules"]

gemnasium-python-dependency_scanning:
  variables:
    DS_MAX_DEPTH: -1
  rules:
    - !reference [".rules_test_dependency_scanning_gemnasium_python", "rules"]

